import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // Activar el pipe
  app.useGlobalPipes(new ValidationPipe({
    whitelist: true, // No toma en cuenta datos diferentes a los existentes en el DTO
    forbidNonWhitelisted: true // Lanza error al enviar datos no existentes en el DTO
  }));
  await app.listen(3000);
}
bootstrap();
